var gulp = require('gulp'),
    jade = require('gulp-jade'),
    jadeInheritance = require('gulp-jade-inheritance'),
    changed = require('gulp-changed'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    browserSync = require("browser-sync"),
    connect = require('gulp-connect'),
    autoprefixer = require('gulp-autoprefixer'),
    svgSymbols = require('gulp-svg-symbols'),
    path = require('path'),
    gulp_if = require('gulp-if'),
    rename = require('gulp-rename'),
    csso = require('gulp-csso'),
    rigger = require('gulp-rigger'),
    del = require('del'),
    zip = require('gulp-zip'),
    cached = require('gulp-cached'),
    filter = require('gulp-filter'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace'),
    plumber = require('gulp-plumber'),
    uglify = require('gulp-uglify'),
    prettify = require('gulp-prettify'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat');

var pkg = require('./package.json');

var outputDir = 'build/',
    outputImgDir = 'build/images/',
    outputSpriteDir = 'build/images/ico/';

var reload = browserSync.reload;

// gulp.task('jade', function(){
//    gulp.src('src/templates/**/*.jade')
//         .pipe(changed(outputDir, {extension: '.html'}))
//         .pipe(gulp_if(global.isWatching, cached('jade')))
//         .pipe(jadeInheritance({basedir: 'src/templates/'}))
//         .pipe(filter(function (file) {
//             return !/\/_/.test(file.path) && !/^_/.test(file.relative);
//         }))
//         .pipe(plumber())
//         .pipe(jade())
//         .pipe(prettify({ indent_size: 4, unformatted: [] }))
//         .pipe(gulp.dest(outputDir))
//         .pipe(connect.reload());
// });

gulp.task('jade', function () {
    return gulp.src('src/templates/**/*.jade')
        .pipe(plumber())
        .pipe(changed(outputDir, { extension: '.html' }))
        .pipe(cached('jade'))
        .pipe(gulp_if(global.isWatching, jadeInheritance({ basedir: 'src/templates/' })))
        .pipe(filter(function (file) {
            return !/\/_/.test(file.path) && !/^_/.test(file.relative);
        }))
        // .pipe(filter(file => /src[\\\/]templates/.test(file.path)))
        .pipe(jade({
            pretty: '\t'
        }))
        .pipe(gulp.dest(outputDir))
        .pipe(reload({ stream: true }));
});

gulp.task('setWatch', function () {
    global.isWatching = true;
});

gulp.task('css', function () {
    return gulp.src('src/sass/main.sass')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(changed('build/css'))
        .pipe(sass({
            css: 'build/css',
            sass: 'src/sass',
            path: '/',
            image: 'build/images',
            style: 'expanded', // nested, expanded, compact, compressed
            comments: false
        }))
        .on('error', function (err) {
            console.log(err);
            // Would like to catch the error here
        })
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 3 versions', 'ie 10', 'safari 8'],
            cascade: false
        }))
        .pipe(csso())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(outputDir + 'css'))
        .pipe(reload({ stream: true }));
});


gulp.task('svg', function () {
    return gulp.src('src/svg/sprites/*.svg')
        // minify svg
        .pipe(svgmin(function (file) {
            var name = path.basename(file.relative, path.extname(file.relative))
            var ids = { cleanupIDs: { minify: true, prefix: 'def-' + name + '-' } }
            return { plugins: [ids] }
        }))
        // remove all fill and style declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))

        .pipe(svgSymbols({
            id: 'icon-%f',
            className: '.icon-%f',
            svgClassname: 'visually-hidden'
        }))
        //.pipe(gulp_if(/[.]css$/, rename('_symbols.scss')))
        //.pipe(gulp_if(/[.]svg$/, gulp.dest('src/svg')))
        //.pipe(gulp_if(/[.]scss$/, gulp.dest('src/sass')))
        .pipe(gulp.dest('src/svg/symbols'))
        .pipe(reload({ stream: true }));
});

gulp.task('svg:scss', ['svg'], function () {
    return gulp.src('src/svg/symbols/*.css')
        .pipe(rename('_symbols.scss'))
        .pipe(gulp.dest('src/sass/'))
});

gulp.task('img', function () {
    return gulp.src(['src/images/**/*'])
        .pipe(gulp.dest(outputImgDir))
        .pipe(reload({ stream: true }));
});

gulp.task('fonts', function () {
    return gulp.src('src/fonts/**/*.*')
        .pipe(gulp.dest(outputDir + "fonts"))
        .pipe(reload({ stream: true }));
});

gulp.task('js', function () {
    return gulp.src('./src/js/main/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(outputDir + 'js'))
        .pipe(reload({ stream: true }));
});

// копирование библиотек
gulp.task('libs', function () {
    return gulp.src('src/libs/**/*.*')
        .pipe(gulp.dest(outputDir + "libs"))
        .pipe(reload({ stream: true }));
});

// Библиотеки js в один файл
gulp.task('vendor', function () {
    return gulp.src('./src/js/vendor/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(concat('vendor.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(outputDir + 'js'))
        .pipe(reload({ stream: true }));
});

// Если нужно удалить грязную верстку
gulp.task('clean', del.bind(null, 'build'));

// Если нужен архивчик с версткой
gulp.task('archive', function () {
    return gulp.src('build/**/*')
        .pipe(zip(pkg.name + '-' + pkg.version + '.zip'))
        .pipe(gulp.dest('.'));
});

gulp.task('watch', ['setWatch', 'jade'], function () {
    gulp.watch('src/templates/**/*.jade', ['jade']);
    gulp.watch('src/svg/sprites/*.svg', ['svg']);
    gulp.watch('src/svg/symbols/*.css', ['svg:scss']);
    gulp.watch('src/sass/**/*.s{a,c}ss', ['css']);
    //gulp.watch('build/css/*', ['css:min']);
    gulp.watch('src/images/**/*', ['img']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/fonts/*', ['fonts']);
    gulp.watch('src/libs/*', ['libs']);
});

// gulp.task('connect', function(){
//     connect.server({
//         root: [outputDir],
//         port: 3000,
//         livereload: true
//     });
// });

gulp.task('webserver', function () {
    browserSync({
        server: {
            baseDir: './build'
        },
        tunnel: false,
        host: 'localhost',
        port: 3000,
        open: false
    });
});

gulp.task('default', ['jade', 'css', 'svg', 'svg:scss', 'fonts', 'libs',  'img', 'vendor', 'js', 'webserver', 'watch']);
