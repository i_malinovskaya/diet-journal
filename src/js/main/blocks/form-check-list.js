$(".form-check-list__title, .form-check-list__text, .form-check-list__image").on("click", function() {
  var $this = $(this).closest('.form-check-list__item');

  if ($this.hasClass("active")) {
    $this.removeClass("active");
  } else {
    $this.addClass("active");
  }
  
  var $check = $this.find(".form-check-list__input");
  console.log('sc', $check)
  $check.prop("checked", !$check.prop("checked"));
});
