$(document).on("click", ".check-group__button", function(event) {
  event.stopPropagation();
  event.preventDefault()
  // if (event.target.tagName === "LABEL") {
    var $this = $(this);
    var $parrent = $this.closest(".check-group");
    if ($parrent.hasClass("check-group--type-checkbox")) {
      $this.toggleClass("active");
      $this
        .find(".check-group__input")
        .prop("checked", !$this.find(".check-group__input").attr("checked"));
    } else {
      $parrent.find(".check-group__button").removeClass("active");
      $parrent.find(".check-group__input").attr("checked", false);
      $this.addClass("active");
      $this.find(".check-group__input").attr("checked", true);
    }
  // } else {
    console.info('check-group__button должен быть тегом label')
  // }
});
