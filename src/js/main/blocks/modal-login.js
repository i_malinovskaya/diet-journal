$(function() {
    var $body = $('body')
    function modalClose() {
        $('.modal-up').removeClass('active')
        $body.removeClass('modal-up-page')
    }

    $(document).on('click', '.js-modal-login', function(e) {
        modalClose()
        $('.modal-up--login').addClass('active')
        $body.addClass('modal-up-page')
        return false;
    })

    $(document).on('click', '.js-modal-recovery', function(e) {
        modalClose()
        $('.modal-up--recovery').addClass('active')
        $body.addClass('modal-up-page')
        return false;
    })

    $(document).on('click', '.js-modal-reset', function(e) {
        modalClose()
        $('.modal-up--reset').addClass('active')
        $body.addClass('modal-up-page')
        return false;
    })

    $(document).on('click', '.js-modal-up-close', function(e) {
        modalClose()
        return false;
    })

    // $('.js-modal-reset').click()
})
