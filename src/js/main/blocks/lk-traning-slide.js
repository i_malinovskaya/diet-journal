var swiper = new Swiper(".lk-traning .swiper-container", {
  slidesPerView: 2,
  spaceBetween: 30,

  pagination: {
    el: ".lk-traning .swiper-pagination",
    clickable: true
  },

  navigation: {
    nextEl: ".lk-traning .swiper-button-next",
    prevEl: ".lk-traning .swiper-button-prev"
  },

  breakpoints: {
    768: {
      slidesPerView: 1,
      spaceBetween: 0
    }
  }
});
