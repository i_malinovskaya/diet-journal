function traningSliderStart() {
  return new Swiper(".journal-theme", {
    pagination: {
      el: ".journal-theme .swiper-pagination",
      type: "fraction",
      renderFraction: function(currentClass, totalClass) {
        return '<span class="' + currentClass + '"></span>';
      }
    },

    navigation: {
      nextEl: ".journal-theme .swiper-button-next",
      prevEl: ".journal-theme .swiper-button-prev"
    }
  });
}
traningSliderStart();

$("#traning-modal").on("shown.bs.modal", function() {
  traningSliderStart()
});
