var mySwiper = undefined;
var breakpointSlider = 1024;
function initSwiper() {
  var screenWidth = $(window).width();
  if (screenWidth < breakpointSlider && mySwiper == undefined) {
    mySwiper = new Swiper(".week-theme", {
      spaceBetween: 0,
      freeMode: true,
      slidesPerView: 3,
      loop: true,
      pagination: {
        el: ".week__pagination",
        type: "bullets",
        bulletClass: "week__bullet",
        bulletActiveClass: "week__bullet--active"
      },

      navigation: {
        nextEl: ".week__next",
        prevEl: ".week__prev"
      },

      breakpoints: {
        1024: {
          slidesPerView: 2,
          spaceBetween: 32
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 32
        }
      }
    });
  } else if (screenWidth > breakpointSlider && mySwiper != undefined) {
    mySwiper.destroy();
    mySwiper = undefined;
    $(".week-theme .swiper-wrapper").removeAttr("style");
    $(".week-theme .swiper-slide").removeAttr("style");
  }
}

//Swiper plugin initialization
initSwiper();

//Swiper plugin initialization on window resize
$(window).on("resize", function() {
  initSwiper();
});
