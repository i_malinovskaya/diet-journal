$(function() {
  var $body = $("body");

  // $('[data-target="#dish-replace"]:first').click();
  // $('[data-target="#wizard-confirm"]').click()
  $body.on(
    "click",
    ".dish--modal .dish__image, .dish--modal .dish__head, .dish--modal .dish__text",
    function() {
      var $this = $(this).closest(".dish--modal");

      if ($this.hasClass("active")) {
        $this.removeClass("active");
      } else {
        $this.addClass("active");
      }

      var $check = $this.find('input[type="checkbox"], input[type="radio"]');
      console.log("sc", $check.prop("checked"));
      $check.prop("checked", !$check.prop("checked"));
    }
  );
});
