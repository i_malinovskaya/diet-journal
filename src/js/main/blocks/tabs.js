// diets tabs
function openDiet(evt, dietName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("diets-tabs__type");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].classList.remove('showed');
    }
    tablinks = document.getElementsByClassName("diets-tabs__button");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(dietName).classList.add('showed');
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
// document.getElementById("defaultOpen").click();



//training tabs
function openTraining(evt, trainingName) {
    var i;
    var tabSection = document.getElementsByClassName("plan-training__type");
    for (i = 0; i < tabSection.length; i++) {
        tabSection[i].classList.remove('showed');
    }
    document.getElementById(trainingName).classList.add('showed');

    var tablinks = document.getElementsByClassName("plan-training__button");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    evt.currentTarget.className += " active";
}

function universalTab(evt, activeTabName) {
    var i;
    var tabSection = document.getElementsByClassName("tabSectionItem");
    for (i = 0; i < tabSection.length; i++) {
        tabSection[i].classList.remove('showed');
    }
    document.getElementById(activeTabName).classList.add('showed');

    var tablinks = document.getElementsByClassName("tabSectionButton");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    evt.currentTarget.className += " active";
}
