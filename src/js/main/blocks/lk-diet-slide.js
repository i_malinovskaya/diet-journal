var swiper = new Swiper(".lk-diet-slide .swiper-container", {
  slidesPerView: 3,
  spaceBetween: 30,

  pagination: {
    el: ".lk-diet-slide .swiper-pagination",
    clickable: true
  },

  navigation: {
    nextEl: ".lk-diet-slide .swiper-button-next",
    prevEl: ".lk-diet-slide .swiper-button-prev"
  },

  breakpoints: {
    1024: {
      slidesPerView: 2
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 20
    }
  }
});
