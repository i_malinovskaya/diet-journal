var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.classList.contains('active')) {
            panel.classList.remove('active')
        } else {
            panel.classList.add('active')
        }
    });
}
console.log('accordion');

// Accordion in FAQ
$('.plan-training-accordeon__toggle').click(function(e) {
    e.preventDefault();

    var $this = $(this);

    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
        $('.icon-trigger-cross').removeClass('open')
    } else {
        $this.parent().parent().find('li .plan-training-accordeon__content').removeClass('show');
        $this.find('.icon-trigger-cross').removeClass('open');
        $this.parent().parent().find('li .plan-training-accordeon__content').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
        $this.find('.icon-trigger-cross').toggleClass('open')
    }
});
